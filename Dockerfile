FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY src/MyApplication.Api/*.csproj ./src/MyApplication.Api/
COPY MyApplication.sln ./
RUN dotnet restore ./MyApplication.sln

# Copy everything else and build
COPY src ./src
RUN dotnet publish ./MyApplication.sln -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "MyApplication.Api.dll"]