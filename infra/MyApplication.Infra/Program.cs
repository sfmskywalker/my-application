﻿using System.Threading.Tasks;
using MyApplication.Infra;
using Pulumi;

class Program
{
    static Task<int> Main() => Deployment.RunAsync<CrowdyStack>();
}
