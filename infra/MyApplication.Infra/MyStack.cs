using Pulumi;
using Pulumi.Docker;

namespace MyApplication.Infra
{
    public class CrowdyStack : Stack
    {
        public CrowdyStack()
        {
            var serviceName = "my-service";

            // Create a docker image.
            var image = new Image(serviceName, new ImageArgs
            {
                Build = "../../",
                LocalImageName = $"{serviceName}",
                ImageName = $"{serviceName}"
            });

        }
    }
}