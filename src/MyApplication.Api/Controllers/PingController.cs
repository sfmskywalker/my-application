﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace MyApplication.Api.Controllers
{
    [Route("{controller}")]
    [ApiController]
    public class PingController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(DateTime.UtcNow);
        }
    }
}